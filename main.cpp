#include "object/World.h"

int main() {
    World monde1(10);   //Initialisation du monde avec 10 lapins
    monde1.run(120);  //Debut de la simulation pour 10 ans (donc 120 mois)

    return 0;
}