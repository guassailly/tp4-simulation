#include <algorithm>
#include "Random.h"

int Random::randomIntMT(int min, int max)
{
    //Distribution uniforme discrete avec Mersen Twister
    std::uniform_int_distribution<int> dist(min, max);
    return dist(mt);
}

double Random::randomDoubleMT(int min, int max)
{
    //Distribution uniforme continue avec Mersen Twister
    std::uniform_real_distribution<double> dist(min, max);
    return dist(mt);
}

double Random::randomGaussianMT(double mean, double deviation)
{
    //Distribution gaussienne avec Mersen Twister
    std::normal_distribution<double> dist(mean, deviation);
    return dist(mt);
}