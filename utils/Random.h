#ifndef TP1_ARCHI_RANDOM_H
#define TP1_ARCHI_RANDOM_H

#include <random>

static std::mt19937_64 mt{12345};   //Initialisation de la seed

class Random {

public:
    //Distribution uniforme avec Mersen Twister
    static int randomIntMT(int min, int max);
    static double randomDoubleMT(int min, int max);

    //Distribution gaussienne avec Mersen Twister
    //for maturityTime : mean = 6.5 and deviation = 1
    static double randomGaussianMT(double mean, double deviation);
};

#endif //TP1_ARCHI_RANDOM_H