//
// Created by aruma on 23/10/2023.
//

#include <iostream>
#include "RabbitWorld.h"



RabbitWorld::RabbitWorld(int mois) {

    rabbit_adult=0;
    rabbit_young=1;

    std::cout   << "nb couple lapin total   : " << (rabbit_adult + rabbit_young) << "\n"
                << "nb couple lapin jeunes  : " << rabbit_young << "\n"
                << "nb couple lapin adultes : " << rabbit_adult << "\n\n";

    int rabbit_grow = 0;
    for(int i = 0; i < mois; i++)
    {
        rabbit_grow = rabbit_young;
        rabbit_young = rabbit_adult;
        rabbit_adult += rabbit_grow;

        std::cout   << "nb couple lapin total   : " << (rabbit_adult + rabbit_young) << "\n"
                    << "nb couple lapin jeunes  : " << rabbit_young << "\n"
                    << "nb couple lapin adultes : " << rabbit_adult << "\n\n";
    }
}
