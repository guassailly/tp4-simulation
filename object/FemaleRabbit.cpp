#include "FemaleRabbit.h"

FemaleRabbit::~FemaleRabbit()
{
    //Destructeur de FemaleRabbit
    //note : Cette méthode appelle le destructeur de la classe mère ~Rabbit()
    nbFemales--;    //Décrémente le nombre de lapins femelles total
}

int FemaleRabbit::newLitter()
{
    //Retourne le nombre de lapin qu'une femelle donne chaque mois (parfois 0)
    double probLitter = ((Random::randomGaussianMT(6, 0.8))/12);    //Tirage d'un nombre selon une gaussienne
    //Ici on tire un nombre avec une moyenne de 6, donc la probabilité d'avoir une portée sera en moyenne 6/12 soit 0.5

    int nbRabbit = 0;
    if(probLitter > Random::randomDoubleMT(0, 1))
    {
        nbRabbit = Random::randomIntMT(3,6);    //Choisi un nombre entre 3 et 6 pour renvoyer le nombre de lapins de la portée
    }
    return nbRabbit;
}