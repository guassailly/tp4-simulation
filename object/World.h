//
// Created by guill on 23/10/2023.
//

#ifndef TP4_WORLD_H
#define TP4_WORLD_H
#include <vector>
#include "Rabbit.h"
#include "FemaleRabbit.h"

using namespace std;

class World {

public:
    World();                //Constructeur par défaut avec 10 lapins
    explicit World(int);    //Constructeur avec un nombre de lapins définit
    void run(int);          //Lance la simulation avec un nombre de mois définit
    void addRabbits(int);   //Met à jour le vecteur "rabbit" avec un nombre de lapins nouveaux nés définit

private:
    int nbRabbitInit;               //Nombre de lapins au debut de la simulation
    std::vector<Rabbit*> rabbits;   //Vecteur contenant des pointeurs vers tout les lapins
};

#endif //TP4_WORLD_H
