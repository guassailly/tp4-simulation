#include "World.h"

World::World(int nbRabbitInit)
{
    //Constructeur avec un nombre de lapins définit

    this->nbRabbitInit = nbRabbitInit;  //Initialise le nombre de lapins
    // (les objets ne sont pas encore crées, donc on ne les compte pas encore)
    Rabbit::nbRabbit = 0;   //Initialise le nombre de lapins total à 0
    Rabbit::nbFemales = 0;  //Initialise le nombre de femelles total à 0
}

World::World()
{
    //Constructeur avec un nombre de lapins définit

    this->nbRabbitInit = 10;    //Initialise le nombre de lapins à 10
    // (les objets ne sont pas encore crées, donc on ne les compte pas encore)
    Rabbit::nbRabbit = 0;   //Initialise le nombre de lapins total à 0
    Rabbit::nbFemales = 0;  //Initialise le nombre de femelles total à 0
}

void World::run(int nbMaxMois)
{
    //Lance la simulation avec un nombre de mois définit

    addRabbits(nbRabbitInit);   //Ajout du nombre de lapins initial

    for(int nbGen = 0; nbGen < nbMaxMois; nbGen++)
    {
        int nbNewRabbit = 0;                    //Initialisation du nombre de nouveaux lapins du mois
        auto it = rabbits.begin();      //Itérateur sur le vecteur contenant tout les lapins
        while (it != rabbits.end())             //Parcours de ce vecteur
        {
            Rabbit *i = *it;

            i->grow();  //Fait grandir et tue des lapins
            if (!i->isAlive())  //Retire le lapin du vecteur de lapins si il est mort
            {
                delete i;
                it = rabbits.erase(it);
            }
            else
            {
                auto * femaleRabbit = dynamic_cast<FemaleRabbit*>(i);    //dynamic cast pour vérifier si le lapin est une femelle
                if((femaleRabbit) && (i->isAdult()) && (Rabbit::nbRabbit - Rabbit::nbFemales > 0))
                {
                    nbNewRabbit += FemaleRabbit::newLitter();   //Ajout d'une nouvelle portée au nombre de nouveaux lapins du mois
                }
                ++it;
            }
        }

        addRabbits(nbNewRabbit);    //Ajout de tout les nouveaux lapins

        cout << "mois [" << nbGen << "]  nbRabbit : " << Rabbit::nbRabbit << endl;  //Affichage du nombre de lapins
    }
}

void World::addRabbits(int nbRabbits)
{
    //Met à jour le vecteur "rabbit" avec un nombre de lapins nouveaux nés définit

    for (int i = 0; i < nbRabbits; i++)
    {
        bool female = Random::randomIntMT(0,1);  //Tirage pour savoir si le nouveau lapin est une femelle
        Rabbit * newRabbit; //Nouveau lapin à ajouter

        if(female)
        {
            newRabbit = new FemaleRabbit(); //Nouvelle femelle
            Rabbit::nbFemales++;    //Incrémente le nombre fe femelles total
        }
        else
        {
            newRabbit = new Rabbit();   //Nouveau mâle
        }
        rabbits.push_back(newRabbit);   //Ajout du lapin au vecteur de lapins
    }
}