#ifndef TP4_FEMALERABBIT_H
#define TP4_FEMALERABBIT_H

#include <vector>
#include "Rabbit.h"

class FemaleRabbit : public Rabbit {

public:
    ~FemaleRabbit();            //Destructeur de FemaleRabbit
    static int newLitter();     //Retourne le nombre de lapin qu'une femelle donne chaque mois (parfois 0)
};

#endif //TP4_FEMALERABBIT_H