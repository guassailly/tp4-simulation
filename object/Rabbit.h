#ifndef TP4_RABBIT_H
#define TP4_RABBIT_H

#include <iostream>
#include "../Utils/Random.h"

class Rabbit {
public :
    Rabbit();           //Constructeur de Rabbit
    virtual ~Rabbit();  //Destructeur de Rabbit

    static int nbRabbit;    //Nombre de lapins total
    static int nbFemales;   //Nombre de lapins femelles

    void grow();    //Fonction executée chaque mois pour mettre à jour les informations du lapin
    bool isAdult(); //Test si age >= maturityAge
    bool isAlive(); //Renvoie alive

protected :
    bool alive;             //true = le lapin est vivant, false = le lapin meirt
    int age;                //Age du lapin en mois
    double survivalRate;    //Chance de survivre chaque mois
    int maturityAge;        //Age ou le lapin deviens adulte
};

#endif //TP4_RABBIT_H