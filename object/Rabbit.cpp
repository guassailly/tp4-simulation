#include "Rabbit.h"

int Rabbit::nbRabbit    = 0;
int Rabbit::nbFemales   = 0;

Rabbit::Rabbit()
{
    nbRabbit++;     //Incrémente le nombre de lapins total
    age     = 0;    //Initialisation de l'âge du lapin à 0
    alive   = true; //Le lapin viens de naître, donc il est vivant
    maturityAge = (int)(Random::randomGaussianMT(6.5, 0.7) + 0.5);  //Initialisation de l'age de maturité du lapin


    //Ajustage des extrémums pour que l'âge de maturité soit entre 5 et 8
    if(maturityAge > 8)
    {
        maturityAge = 8;
    }
    else if(maturityAge < 5)
    {
        maturityAge = 5;
    }

    survivalRate = pow(0.35,1.0/12.0);  //Definition de la probabilité de mourir chaque mois pour un jeune lapin
}

Rabbit::~Rabbit()
{
    nbRabbit--; //Décrémente le nombre de lapins total
}

void Rabbit::grow() {
    //Fonction executée chaque mois pour mettre à jour les informations du lapin

    //Test de l'âge pour mettre à jour les chances de survie par mois
    if(age == maturityAge)  //Si le lapin deviens adulte
    {
        survivalRate = pow(0.60,1.0/12.0);
    }
    else if((age > 120) && (age < 180)) //Si le lapin vieillit
    {
        survivalRate = pow(0.60,1.0/12.0) * pow(0.9,(age - 120.0)/12);
    }
    else if(age == 180) //Si le lapin atteint la limite biologique imposée par l'énoncé
    {
        survivalRate = 0;
    }

    double die = Random::randomDoubleMT(0,1);   //Tirage aléatoire pour définir si le lapin meurt
    if(die > survivalRate)
    {
        alive = false;
    }

    age++;  //Le lapin grandit d'un mois
}

bool Rabbit::isAdult()
{
    //Test si age >= maturityAge

    bool adult = false;
    if(age >= maturityAge)
    {
        adult = true;
    }

    return adult;
}

bool Rabbit::isAlive()
{
    return alive;
}